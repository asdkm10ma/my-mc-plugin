package com.tewahh;

import com.tewahh.commands.HelloCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

   @Override
   public void onEnable() {
      new HelloCommand(this);
   }
}